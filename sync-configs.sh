#!/bin/sh

set -eu

rm -v "$HOME"/.zshenv 2> /dev/null || true

cat <<-EOF > "$HOME/.zshenv"
	#!/bin/zsh

	# In the case that ZDOTDIR has not been set to ~/.config/zsh
	# this ~/.zshenv is sourced and here we source the actual .zshenv
	# too to not miss/skip it.

	export ZDOTDIR="\$HOME/.config/zsh"

	. "\$ZDOTDIR/.zshenv"
EOF

#ln -srf "$HOME/.config/zsh/.zshenv" "$HOME/.zshenv"
#ln -srf "$HOME/.config/zsh/.zshrc" "$HOME/.zshrc"

mkdir -p "$HOME/.config/zsh/.cache"
