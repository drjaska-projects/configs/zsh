#!/bin/zsh

# Dr. Jaska's zshrc

# {{{ Env Vars

# Partly in $ZDOTDIR/.zshenv

# $HOME/.zshenv SHOULD dictate $ZDOTDIR
# If we don't have a $ZDOTDIR like from .zshenv then
# default to $HOME/.config/zsh
# with backup of $HOME/.zsh
if [ -z "$ZDOTDIR" ]
then
	if [ -d "$HOME/.config/zsh"] && \
		export ZDOTDIR="$HOME/.config/zsh/"

	if [ -d "$HOME/.zsh"] && \
		export ZDOTDIR="$HOME/.zsh/"
fi

# my plugin path and oh my zsh install location
export ZSHPLUGINPATH="$ZDOTDIR/plugins"
export ZSH="$ZSHPLUGINPATH/ohmyzsh"

export ZSH_CACHE_DIR="$ZDOTDIR/.cache"

# Keep 100 000 lines of shell history
HISTSIZE=100000
SAVEHIST=100000
export HISTFILE="$ZDOTDIR/.zsh_history"

# set up colors
autoload colors
colors

# }}}

# {{{ Options

# HISTIGNOREALLDUPS don't list duplicate entries into history database
# SHAREHISTORY history is shared between sessions/terminal windows
# INTERACTIVE_COMMENTS shell is able to comment and doesn't treat # as a non-comment character
# NO_HUP don't send Hang UP signal or kill background/suspended processes when exiting shell
# GLOB_DOTS don't require a leading .dot in a filename to be matched explicitly
# NOBEEP avoid termbells
setopt histignorealldups interactive_comments no_hup glob_dots nobeep

# SHAREHISTORY appends and imports history live instead of only when exiting
# INCAPENDHISTORY appends history live but does not import it
# ^ mutually exclusive along with INCAPPENDHISTORYTIME
setopt incappendhistory

# Interesting:
# SUN_KEYBOARD_HACK If a line ends with a backquote, and there are an
# odd number of backquotes on the line, ignore the trailing backquote.
# This is useful on some keyboards where the return key is too small,
# and the backquote key lies annoyingly close to it. As an alternative
# the variable KEYBOARD_HACK lets you choose the character to be removed.

# store completion cache to $ZSH_CACHE_DIR/.zcompcache
zstyle ':completion:*' cache-path '$ZSH_CACHE_DIR/.zcompcache'

# }}}

# {{{ Keybinds

# select emacs keymap and bind it to main
# zsh now uses emacs keybindings even if our EDITOR is set to vim
source "${ZSHPLUGINPATH}/ohmyzsh/lib/key-bindings.zsh"

# [Control-Right] and [Alt-Right] forward-word
bindkey "^[[1;5C" forward-word
bindkey "^[[1;3C" forward-word

# [Control-Left] and [Alt-Left] backward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[1;3D" backward-word

# [Ctrl-U] kill line before cursor
# by default zsh emacs binds kill the whole line,
# before and after, while readline/bash only kill before cursor
bindkey "^U" backward-kill-line

# expand glob expressions, subcommands and aliases
# blacklist
GLOBALIAS_FILTER_VALUES=()
source "$ZDOTDIR/lib/globalias/globalias.plugin.zsh"
# [Control-Space] expand all aliases, including global
bindkey -M emacs "^ " globalias
bindkey -M viins "^ " globalias
bindkey -M vicmd "^ " globalias

# }}}

# {{{ Completion files

# additional completions from man pages with https://github.com/nevesnunes/sh-manpage-completions
#[[ "${fpath[*]}" == *"$HOME/programs/sh-manpage-completions/completions/zsh"* ]] || \
#	[ -d "$HOME/programs/sh-manpage-completions/completions/zsh" ] && \
#		fpath+=("$HOME/programs/sh-manpage-completions/completions/zsh")

# additional completions from man pages with https://github.com/umlx5h/zsh-manpage-completion-generator
# seems better than above, less faulty files
[[ "${fpath[*]}" == *"$HOME/.local/share/zsh/generated_man_completions"* ]] || \
	[ -d "$HOME/.local/share/zsh/generated_man_completions" ] && \
		fpath+=("$HOME/.local/share/zsh/generated_man_completions")

# Xonotic git root repository all-script autocompletion
[[ "${fpath[*]}" == *"$HOME/xonotic-git/misc/tools/all/zsh_autocompletion/all" ]] || \
	[ -d "$HOME/xonotic-git/misc/tools/all/zsh_autocompletion/all" ] && \
		fpath+=("$HOME/xonotic-git/misc/tools/all/zsh_autocompletion/all")

# Completions for my bins
[[ "${fpath[*]}" == *"$HOME/bin/completions/zsh" ]] || \
	[ -d "$HOME/bin/completions/zsh" ] && \
		fpath=("$HOME/bin/completions/zsh" $fpath)

# add dirs to fpath BEFORE compinit to load their zsh completion files

# use modern completion system
# +X loads the module without executing it,
# if loading is succesful then execute it
autoload -U +X compinit && compinit -u -d "$ZSH_CACHE_DIR/.zcompdump"
zstyle ':completion:*' use-compctl false

# load completion stuff, bashcompinit and accept sourcing bash completion files
source "${ZSHPLUGINPATH}/ohmyzsh/lib/completion.zsh"

# source bash completion files AFTER loading compinit and bashcompinit

# find dirs used for sysadmin and user installed bash completion files
bashcompletiondirs=()
[ -d "/usr/local/share/bash-completion/completions" ] && \
	bashcompletiondirs+=("/usr/local/share/bash-completion/completions")
[ -d "$HOME/.local/share/bash-completion/completions" ] && \
	bashcompletiondirs+=("$HOME/.local/share/bash-completion/completions")
# causes issues for some reason?
#[ -d "/usr/share/bash-completion/completions" ] && \
#	bashcompletiondirs+=("/usr/share/bash-completion/completions")

# source files found in them if the array isn't empty
# checking avoids find starting to source every file found in ~/  :)
if [ ${#bashcompletiondirs[@]} -ne 0 ]
then
	for file in $(find $bashcompletiondirs -type f)
	do
		source "$file"
	done
fi

# }}}

# {{{ Completion and Expansion options

# load complist module
# it's already loaded above by completion.zsh
#zmodload zsh/complist

# output Bourne shell code to set LS_COLORS
eval "$(dircolors -b)"

#####################################################################
#   :completion:<function>:<completer>:<command>:<argument>:<tag>   #
#####################################################################

# import GNU ls's colors for complist module
# colored completion (different colors for dirs/files/etc)
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# use a menu to select completion entries
zstyle ':completion:*' menu select=2

# complete in word and not just assuming that cursor is at word end
setopt completeinword

# ignore completion functions for commands you don't have:
zstyle ':completion::(^approximate*):*:functions' ignored-patterns '_*'

#requires some fixing if this even is good
#setopt correct_all

# show hidden folders/files in completion list
zstyle ':completion:*' hidden false

# provide .. as a completion
zstyle ':completion:*' special-dirs ..

# automatically find new executables in path
zstyle ':completion:*' rehash true
# run rehash on completion so new installed program are found automatically:
function _force_rehash () {
    (( CURRENT == 1 )) && rehash
    return 1
}

# _expand completer adds a / after valid directory or space after valid filename, command etc.
zstyle ':completion:*' add-space false

# don't complete backup files as executables
zstyle ':completion:*:complete:-command-::commands' ignored-patterns '(aptitude-*|*\~)'

# define message formats for messages from completion
zstyle ':completion:*:warnings'     format 'No completion matches with given input'
zstyle ':completion:*:corrections'  format '%B%d (errors: %e)%b'
zstyle ':completion:*:messages'     format '%d'
zstyle ':completion:*:options'      description 'yes'
zstyle ':completion:*:options'      auto-description '%d'
zstyle ':completion:*:descriptions' format $'%F{red}-- completing %B%d%b%F{red} --%f'
                                                  # idk why unbolding %b cancels %F
zstyle ':completion:*'              verbose true

# group completion list by entry types
zstyle ':completion:*:*:-command-:*:commands' \
	group-name commands
zstyle ':completion:*:*:-command-:*:functions' \
	group-name functions
# ^ but don't group
#zstyle ':completion:*' group-name ''

# pasting with tabs doesn't perform completion
zstyle ':completion:*' insert-tab pending

# with commands like `rm' it's annoying if one gets offered the same filename
# again even if it is already on the command line. To avoid that:
zstyle ':completion:*:rm:*' ignore-line yes

# completers will try to match: exact matches, then case sensitive matches,
# then case insensitive matches, then correction, then partial word completions
#zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'

# completers will try to match: exact matches,
# then case insensitive matches, then correction, then partial word completions
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'

# Search path for sudo completion. Default to Debian's current secure_path
# secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin \
                                           /usr/local/bin  \
                                           /usr/sbin       \
                                           /usr/bin        \
                                           /sbin           \
                                           /bin

# Which completers should be called
#
# Unused completers:
#
#     _all_matches
#         "This  completer can be used to add a
#         string consisting of all other matches."
#
#     _canonical_paths
#         I don't think I have any hot files
#         which I would want to access via this
#
#     _cmdambivalent
#         I didn't understand this one
#
#     _cmdstring
#         Completes an external command as a single argument,
#         as for system(...).
#         I didn't understand what niceties I could do with this
#
#     _expand
#         Instead of "expand the word under the cursor"
#         this defines the behavior of other completers.
#         I currently don't know why I would want to use this
#
#     _expand_alias
#         "If  the  word  the  cursor  is  on  is an alias,
#         it is expanded and no other completers are called."
#         I don't want this
#
#     _external_pwds
#         Why would I complete paths of my other shell PWDs
#
#     _ignored
#         I don't use ignored-patterns style
#         so this has nothing to re-enable AFAIK
#
#     _match
#         Requires GLOB_COMPLETE shell option
#         to work as this implements it.
#         Bad completer because * becomes a single
#         entry menu selection instead of all of them,
#         this would be fixed by _all_matches but I don't
#         know why I would want to use either of these
#
#     _menu
#         probably useless for me with my current configs
#         as I use menu already a lot
#
#     _oldlist
#         all of the completions which were previously
#         generated by a separately bound completion command.
#         I don't have a separate completion bind/cmd
#
#     _precommand
#         "Complete an external command in word-separated
#         arguments, as for exec and /usr/bin/env."
#         I didn't understand what this does
#
# Notes about used ones:
#
#     _prefix
#         attempt completion which ignores everything
#         after the cursor with configured completers
#
zstyle ':completion::prefix:*' completer \
				_complete \
				_list \
				_correct \
				_approximate

# I think these may be default, oh well might as well be explicit
zstyle ':completion:*:correct:::' max-errors 2 not-numeric
zstyle ':completion:*:approximate:::' max-errors 3 numeric

completers=(
	# general completer or something like that
	_complete

	# delay insertion until second insertion with no word change
	_list

	# if cursor is at '*.' then complete file extensions
	_extensions

	# fix typos
	_correct

	# fix typos even more leniently I guess
	_approximate

	# previously explained above
	_prefix

	# search words from history
	_history

	# refresh bins available in PATH
	_force_rehash
)

zstyle ':completion:*' completer "${completers[@]}"

unset completers

# }}}

# {{{ aliases and functions

# ctags alias for Xonotic
alias xontags="ctags -h=".c,.h,.qc,.qh" --language-force="C" -R qcsrc ../../darkplaces"

# aliases to rerun feh for a wallpaper change
alias feh-randomwp="~/.config/feh/fehbg.sh"
alias feh-bluewp="feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/Windows.png"
alias feh-blackwp="feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/black_background.png"
alias feh-saimaawp="feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/saimaa.jpg"
alias feh-motiwp="feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/Moti2.jpg"
alias feh-smileywp="feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/smiley.jpg"
alias feh-bingwp="~/.config/feh/bing-daily-wallpaper.sh \
	&& feh --no-fehbg --bg-scale ~/.config/feh/Wallpapers/bing-daily-wallpaper.jpeg"

# get the number of lines/columns of current terminal window
alias lines="tput lines"
alias cols="tput cols"

alias clearfile=":>"

# don't replace files without prompting
alias mv="mv -i"

# system info prints
alias uptimee="uptime -p && uptime"
alias dff="df -Th | grep -v tmpfs"

# no CD album covers on a separate window
alias mpv="mpv --no-audio-display"

# yt-dlp log and download something using the best available format
alias yt-dlp-archive-video="noglob cl noglob yt-dlp --download-archive archive.txt"
alias yt-dlp-archive-audio="noglob cl noglob yt-dlp --download-archive archive.txt --extract-audio"
yt-dlp-archive-replace () {
	files="$(find . -name "*$1*")"
	if [[ "$(wc -l <<< "$files")" == "1" ]]
	then
		rm -v "$(find . -name "*$1*")"
		cl noglob yt-dlp --extract-audio 'https://www.youtube.com/watch?v='"$1"
	else
		echo several
	fi
}

alias   n="nnn -dinUrH"
alias nnn="nnn -dinUrH"

alias gdiff="git diff --no-index"

alias tmuxi="tmux attach || tmux"
alias screeni="screen -x || screen"

alias weather="curl 'https://wttr.in?M'"

alias v="\$EDITOR"
alias vi="\$EDITOR"
alias :q="exit"

alias setbranchorigin="git branch --set-upstream-to=origin/\$(git branch --show-current) \$(git branch --show-current)"
alias pushbranchorigin="git push --set-upstream origin \$(git branch --show-current)"

# words to lines
# separate space/tab/null separated words with newline instead
alias wtl="tr ' \t\0' '\n'"

#login as root using current user's zsh configs
alias rootlogin="\sudo --preserve-env \$SHELL"

# sudoers secure_path override
alias sudoe="sudo env PATH=\"\$PATH\""
alias SE="sudo env PATH=\"\$PATH\""

alias sudo="\sudo --preserve-env"

alias  valot="ssh -t mari '~/ohjaa' jaskamode false"
alias pimeys="ssh -t mari '~/ohjaa' jaskamode true"

ffmpeg-gif-to-mp4 () {
	# https://unix.stackexchange.com/a/294892
	#
	# movflags
	#     This option optimizes the structure of the MP4 file so the browser
	#     can load it as quickly as possible.
	# pix_fmt
	#     MP4 videos store pixels in different formats. We include this option
	#     to specify a specific format which has maximum compatibility
	#     across all browsers.
	# vf
	#     MP4 videos using H.264 need to have a dimensions that are divisible
	#     by 2. This option ensures that’s the case.
	case "$1" in
		*.gif)
			ffmpeg -i "$1" -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" "${1%.gif}.mp4"
			;;
		*)
			"$0: usage: $0 foobar.gif"
			;;
	esac
}

retryuntil () {
	if [ "$2" = "" ]
	then
		echo "Usage: $0 sleeptime command"
	fi

	local sleeptime="$1"
	shift

	until $@
	do
		sleep "$sleeptime"
	done
}

# recursively find all dirs with .git and git pull them
rpull () {
	find . -name ".git" -print
	printf 'Do you wish to git pull every one of these repos? (Y/n): '
	stty raw
	REPLY=$(dd bs=1 count=1 2> /dev/null)
	stty -raw
	echo ''
	case "$REPLY" in
		[Nn]|"$(printf '\e')") return ;;
		[Yy]|"$(printf '\r')"|"$(printf '\n')"|" ") \
			find . -name ".git" -print -execdir git pull \; ;;
		*) printf '%s\n' 'Please answer y or n.' ;;
	esac
	# N, n, esc = no
	# Y, y, carriage return, newline, space = yes
}

cargo-binaries-stable-update-all () {
	if [ "$1" = "" ]
	then
		cargo install-update -a
	else

		for crate in $(cargo install-update --list | grep "Yes" | cut -d " " -f 1)
		do
			# & for multithreading
			cargo install-update "$crate" &
		done

		# wait for them to finish
		wait
	fi
}

cargo-git-install () {
	if [ "$1" != "" ] && [ "$2" != "" ]
	then
		echo "Usage: $0 gitRemoteURL branchName"
		exit 1
	else
		cargo install --locked --all-features --git "$1" --branch "$2"
	fi
}

# edit something, in new window when using kitty
e () {
	if [ "$TERM" = "xterm-kitty" ]
	then
		if [ -z "$KITTY_PID" ]
		then # non-local, edit-in-kitty if SSH for local editor
			KITTY_CLONE_SOURCE_CODE="exec kitten edit-in-kitty $@" clone-in-kitty --type=os-window
		else
			KITTY_CLONE_SOURCE_CODE="exec $EDITOR $@" clone-in-kitty --type=os-window
		fi
	else
		$EDITOR "$@"
	fi
}

# clone the current session
cl () {
	if [ "$TERM" = "xterm-kitty" ]
	then
		KITTY_CLONE_SOURCE_CODE="$@" clone-in-kitty --type=os-window
	else
		eval "$@"
	fi
}

s () {
	if [ -n "$KITTY_PID" ]
	then
		if [ -n "$DISPLAY" ]
		then
			kitten ssh -XY "$@"
		else
			kitten ssh "$@"
		fi
	else
		ssh "$@" # fallback to below
	fi
}
ssh () {
	if [ -n "$DISPLAY" ]
	then
		command ssh -XY "$@"
	else
		command ssh "$@"
	fi
}

rg () {
	# select grep tool
	if [ "$TERM" = "xterm-kitty" ] \
	&& [ -n "$(command -pv kitty)" ] \
	&& [ -n "$(command -pv rg)" ]
	then
		GREPCMD=(kitten hyperlinked_grep)

	elif [ -n "$(command -pv rg)" ]
	then
		GREPCMD=(command rg)

	else
		GREPCMD=(grep --color=auto -InrE)
	fi

	# check if search contains an upper case letter
	if [[ "$1" =~ [A-Z] ]]
	then # if yes then case specific
		${GREPCMD} "$@"
	else # if not then ignore case
		${GREPCMD} -i "$@"
	fi
}

icat () {
	kitten icat "$@"
}

# create a new folder and move there
take () {
	mkdir -p "$1"
	cd "$1"
}

# create a temp dir and move there
taketemp () {
	cd "$(mktemp -d)"
	pwd
}

# clone a git repository and move there
takegit () {
	git clone "$1"
	cd "$(basename ${1%.git})"
}

# https://paste.rs/
# duplicate with sprunge
uploadpasters () {
	local file="${1:-/dev/stdin}"
	curl --silent --data-binary @"${file}" https://paste.rs | xargs echo
}
deletepasters () {
	if [ -z "$1" ]
	then
		echo "Give an ID to delete"
		return 1
	fi

	# accepts both '12345' and 'https://paste.rs/12345'
	# for ID input, parse ID from URI here with sed
	local id="$(sed -e 's#\(.*\)/\(.*\)#\2#' <<< "$1")"
	curl --silent -X DELETE https://paste.rs/$id
}

saver-cbonsai () {
	cbonsai -S -w1 -t0.02 \
		-L"$(expr $(tput lines) \* 15 / 10)" \
		-M"$(expr $(tput lines) \* 20 / 100)"
}

# get the value of an alias.
alias_value () {
	(( $+aliases[$1] )) && echo $aliases[$1]
}

# git log aliases for showing branch topology well
alias lg="lg1"
alias lg1="git-log1-specific"
alias lg2="git-log2-specific"
alias lg3="git-log3-specific"
alias lga="lga1"
alias lga1="git-log1-specific --all"
alias lga2="git-log2-specific --all"
alias lga3="git-log3-specific --all"

alias git-log1-specific="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'"
alias git-log2-specific="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)'"
alias git-log3-specific="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(auto)%d%C(reset)%n''          %C(white)%s%C(reset)%n''          %C(dim white)- %an <%ae> %C(reset) %C(dim white)(committer: %cn <%ce>)%C(reset)'"

# This script was automatically generated by the broot program
# More information can be found in https://github.com/Canop/broot
# This function starts broot and executes the command
# it produces, if any.
# It's needed because some shell commands, like `cd`,
# have no useful effect if executed in a subshell.
function br {
    local cmd cmd_file code
    cmd_file=$(mktemp)
    if broot --outcmd "$cmd_file" "$@"; then
        cmd=$(<"$cmd_file")
        command rm -f "$cmd_file"
        eval "$cmd"
    else
        code=$?
        command rm -f "$cmd_file"
        return "$code"
    fi
}

# }}}

# {{{ ohmyzsh Plugins

# omz plugins without a .plugin.zsh file to import:
# gas plugin, git author manager, http://github.com/walle/gas
# httpie(friendlier cURL replacement) aliases
# better autocompletion for ripgrep, grep rewritten in rust
# copy ssh autocompletion for mosh
plugins=(gas httpie ripgrep mosh)
source "${ZSHPLUGINPATH}/ohmyzsh/lib/clipboard.zsh"
source "${ZSHPLUGINPATH}/ohmyzsh/lib/directories.zsh"

alias c="clipcopy"
alias p="clippaste"
ci () {
	if [ "${1-}" != "" ]
	then
		MIMETYPE="$(mimetype "$1")"
		cat "$1" | xclip -sel clip -t "${MIMETYPE#*: }"
	else
		# just hope that it's a png...
		cat /dev/stdin | xclip -sel clip -t image/png
	fi
}
# some jpgs are fucked, just treat them as pngs
cip () {
	# just hope that it's a png...
	cat "${1-"/dev/stdin"}" | xclip -sel clip -t image/png
}
pi () {
	# hardcode, neither xsel nor xclip
	# detect image metadata from input?
	xclip -o -sel clip -t image/png
}

# redifine aliases from directories.zsh
alias l='ls -A'
alias l1='ls -A1'
alias ll='ls -lAh'
alias lld='ls -lAhd'
if [ "$TERM" = "xterm-kitty" ]
then
	alias ls='ls --color=auto --hyperlink=auto'
else
	alias ls='ls --color=auto'
fi

# a bunch of git aliases
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/git/git.plugin.zsh"
alias gdwd="git diff --word-diff --word-diff-regex='\w+'"
alias boop="gc -am "boop" && gp"

# a few more handy aliases and functions
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/systemadmin/systemadmin.plugin.zsh"
alias mkdir='mkdir -pv'
function ip() {
  if [ -t 1 ]; then
    command ip -color "$@"
  else
    command ip "$@"
  fi
}

# alias lister
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/aliases/aliases.plugin.zsh"

# alias reminder
#export ZSH_ALIAS_TIPS_TEXT="Alias tip: "
#export ZSH_ALIAS_TIPS_EXCLUDES=""
#export ZSH_ALIAS_TIPS_EXPAND=1
#export ZSH_ALIAS_TIPS_REVEAL=0
#export ZSH_ALIAS_TIPS_REVEAL_TEXT="Alias for: "
#export ZSH_ALIAS_TIPS_FORCE=0
source "$ZDOTDIR/lib/alias-tips/alias-tips.plugin.zsh"
alias alias-show="export ZSH_ALIAS_TIPS_REVEAL=1"
alias alias-hide="export ZSH_ALIAS_TIPS_REVEAL=0"

# find aliases containign things
# "alias-finder ls --longer" finds all alias meanings containing ls and more
# alias-finder ls --longer finds:
# l='ls -lah' , la='ls -lAh' and more
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/alias-finder/alias-finder.plugin.zsh"

# git automatically fetch repositories in the background
# DO NOT USE git push --force-with-lease WITH THIS PLUGIN
# IT WILL OVERWRITE REMOTE HISTORY LIKE FORCE PUSH
#GIT_AUTO_FETCH_INTERVAL="600" # in seconds
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/git-auto-fetch/git-auto-fetch.plugin.zsh"

# keep track of the last working directory inside $HOME and make it new shell's WD
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/last-working-dir/last-working-dir.plugin.zsh"

# autojump for faster directory navigation
# j <dir>
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/autojump/autojump.plugin.zsh"

# Shift+Control+ Left/Right cycles directory stack
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/dircycle/dircycle.plugin.zsh"

# interactive tab completion for cd
# Press tab for completion as usual, it'll launch fzf automatically.
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh"

# integrate fzf into zsh so that it doesn't have to be used as a seperate process
# key bindings (CTRL-T, CTRL-R, ALT-C)
# DISABLE_FZF_KEY_BINDINGS="true"
if [ "$distro" != "termux" ]
then
	if [ -e /usr/share/fzf/key-bindings.zsh ]
	then
		# Arch
		export FZF_BASE=/usr/share/fzf/
	else
		# Debian
		export FZF_BASE=/usr/bin/
	fi
else
	# Termux
	export FZF_BASE="$PREFIX/usr/bin/"
fi
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/fzf/fzf.plugin.zsh"

# web search from CLI. `ddg` searches with duckduckgo, supports bangs.
# requires ohmyzsh's url_encode and open_command functions as dependencies
# those 2 are defined at the end of this file after plugins
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/web-search/web-search.plugin.zsh"

# this plugin defines an `extract` function that extracts the archive file
# you pass it, and it supports a wide variety of archive filetypes.
# this way you don't have to know what specific command extracts a file
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/extract/extract.plugin.zsh"

# when on CLI without an active process and with a suspended process
# Ctrl+z run fg to bring up the suspended process making Ctrl+z a toggle
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/fancy-ctrl-z/fancy-ctrl-z.plugin.zsh"

# safe paste prevents execution of commands when pasting them to CLI
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/safe-paste/safe-paste.plugin.zsh"

# CLI pastebin which uploads to sprunge.us
# duplicate with paste.rs
# Command                    | Description
# sprunge "this is a string" | Uploads plain text string
# sprunge filename.txt       | Uploads filename.txt, invalid paths are strings
# sprunge < filename.txt     | Redirects filename.txt content to sprunge
# echo data | sprunge        | Any piped data will be uploaded
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/sprunge/sprunge.plugin.zsh"

# sets title and hardstatus of the tab window for screen
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/screen/screen.plugin.zsh"

# press esc twice to prefix with sudo
# takes the previous command and prefixes
# it with sudo when done with an empty line
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/sudo/sudo.plugin.zsh"

# colored man pages
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/colored-man-pages/colored-man-pages.plugin.zsh"

# suggest packages which install the command if it's not found currently
source "${ZSHPLUGINPATH}/ohmyzsh/plugins/command-not-found/command-not-found.plugin.zsh"

# Advanced command-not-found hook
#[ -e /usr/share/doc/find-the-command/ftc.zsh ] && \
#	source /usr/share/doc/find-the-command/ftc.zsh

# background notifications for long running commands
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/bgnotify/bgnotify.plugin.zsh"

# alternative notification to bgnotify
# default duration: 15s
#zbell_duration=6
# blacklist
#zbell_ignore=($EDITOR $PAGER)
# display a popup on the screen?
#zbell_use_notify_send="false"
#source "${ZSHPLUGINPATH}/ohmyzsh/plugins/zbell/zbell.plugin.zsh"

# custom prompt theme
#ZSH_THEME="rkj-repos"
#source "${ZSHPLUGINPATH}/ohmyzsh/themes/rkj-repos.zsh-theme"

# }}}

# {{{ non-ohmyzsh Plugins

# set a theme
if [ "$EUID" != "0" ] \
&& command -v starship &> /dev/null \
&& [ -e "${ZSHPLUGINPATH}/starship/starship.toml" ]
then
	export STARSHIP_CONFIG="${ZSHPLUGINPATH}/starship/starship.toml"
	eval "$(starship init zsh)"

elif [ -e /usr/share/powerlevel9k/powerlevel9k.zsh-theme ]
then
	if true
	then
		POWERLEVEL9K_DISABLE_RPROMPT=true
		POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context vcs newline status dir dir_writable)
		#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS+=(vi_mode)
	else
		POWERLEVEL9K_PROMPT_ON_NEWLINE=true
		POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
		POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context vcs)
		POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status dir dir_writable)
		#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS+=(vi_mode)
	fi
	source  /usr/share/powerlevel9k/powerlevel9k.zsh-theme
fi

# automatically execute ls and git diff&status when changing dirs
# with hyperlinks in kitty
if [ "$TERM" = "xterm-kitty" ]
then
	AUTO_LS_COMMANDS+=(ls-hyperlink)
else
	AUTO_LS_COMMANDS+=(ls)
fi
AUTO_LS_COMMANDS+=(git-fetch-all git-diff-status)
source "$ZDOTDIR/lib/auto-ls/auto-ls.plugin.zsh"
#source "${ZSHPLUGINPATH}/auto-ls/auto-ls.plugin.zsh"

# suggest line completions based on shell history
zmodload zsh/zpty
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
# FIXME: "completion" doesn't work due to ??
if [ "$EUID" != "0" ] && [ -e "${ZSHPLUGINPATH}/zsh-autosuggestions/zsh-autosuggestions.zsh" ]
then
	source "${ZSHPLUGINPATH}/zsh-autosuggestions/zsh-autosuggestions.zsh"

elif [ -e /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ]
then
	source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

elif [ -e /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh ]
then
	source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
fi

# live syntax highlighting for prompt input
if [ "$EUID" != "0" ] && [ -e "${ZSHPLUGINPATH}/zdharma-continuum/fast-syntax-highlighting" ]
then
	source "${ZSHPLUGINPATH}/zdharma-continuum/fast-syntax-highlighting"

elif [ "$EUID" != "0" ] && [ -e "${ZSHPLUGINPATH}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" ]
then
	source "${ZSHPLUGINPATH}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

elif [ -e /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]
then
	source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

elif [ -e /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]
then
	source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# terminal's window's WM_NAME will include zsh:activeprocess like zsh:vim
# this carries over to xmobar to display currently focused window's title
#ZSH_TAB_TITLE_CONCAT_FOLDER_PROCESS=true
#source "${ZSHPLUGINPATH}/zsh-tab-title/zsh-tab-title.plugin.zsh"

# }}}

# {{{ ohmyzsh functions for web-search dependencies

# Required for $langinfo
zmodload zsh/langinfo

# URL-encode a string
#
# Encodes a string using RFC 2396 URL-encoding (%-escaped).
# See: https://www.ietf.org/rfc/rfc2396.txt
#
# By default, reserved characters and unreserved "mark" characters are
# not escaped by this function. This allows the common usage of passing
# an entire URL in, and encoding just special characters in it, with
# the expectation that reserved and mark characters are used appropriately.
# The -r and -m options turn on escaping of the reserved and mark characters,
# respectively, which allows arbitrary strings to be fully escaped for
# embedding inside URLs, where reserved characters might be misinterpreted.
#
# Prints the encoded string on stdout.
# Returns nonzero if encoding failed.
#
# Usage:
#  omz_urlencode [-r] [-m] [-P] <string> [<string> ...]
#
#    -r causes reserved characters (;/?:@&=+$,) to be escaped
#
#    -m causes "mark" characters (_.!~*''()-) to be escaped
#
#    -P causes spaces to be encoded as '%20' instead of '+'
function omz_urlencode() {
  emulate -L zsh
  local -a opts
  zparseopts -D -E -a opts r m P

  local in_str="$@"
  local url_str=""
  local spaces_as_plus
  if [[ -z $opts[(r)-P] ]]; then spaces_as_plus=1; fi
  local str="$in_str"

  # URLs must use UTF-8 encoding; convert str to UTF-8 if required
  local encoding=$langinfo[CODESET]
  local safe_encodings
  safe_encodings=(UTF-8 utf8 US-ASCII)
  if [[ -z ${safe_encodings[(r)$encoding]} ]]; then
    str=$(echo -E "$str" | iconv -f $encoding -t UTF-8)
    if [[ $? != 0 ]]; then
      echo "Error converting string from $encoding to UTF-8" >&2
      return 1
    fi
  fi

  # Use LC_CTYPE=C to process text byte-by-byte
  local i byte ord LC_ALL=C
  export LC_ALL
  local reserved=';/?:@&=+$,'
  local mark='_.!~*''()-'
  local dont_escape="[A-Za-z0-9"
  if [[ -z $opts[(r)-r] ]]; then
    dont_escape+=$reserved
  fi
  # $mark must be last because of the "-"
  if [[ -z $opts[(r)-m] ]]; then
    dont_escape+=$mark
  fi
  dont_escape+="]"

  # Implemented to use a single printf call and avoid subshells in the loop,
  # for performance (primarily on Windows).
  local url_str=""
  for (( i = 1; i <= ${#str}; ++i )); do
    byte="$str[i]"
    if [[ "$byte" =~ "$dont_escape" ]]; then
      url_str+="$byte"
    else
      if [[ "$byte" == " " && -n $spaces_as_plus ]]; then
        url_str+="+"
      else
        ord=$(( [##16] #byte ))
        url_str+="%$ord"
      fi
    fi
  done
  echo -E "$url_str"
}

function open_command() {
  local open_cmd

  # define the open command
  case "$OSTYPE" in
    darwin*)  open_cmd='open' ;;
    cygwin*)  open_cmd='cygstart' ;;
    linux*)   [[ "$(uname -r)" != *icrosoft* ]] && open_cmd='nohup xdg-open' || {
                open_cmd='cmd.exe /c start ""'
                [[ -e "$1" ]] && { 1="$(wslpath -w "${1:a}")" || return 1 }
              } ;;
    msys*)    open_cmd='start ""' ;;
    *)        echo "Platform $OSTYPE not supported"
              return 1
              ;;
  esac

  ${=open_cmd} "$@" &>/dev/null
}

# }}}

# {{{ Startup commands

# {{{ Arch BTW

[ "$distro" = "arch" ] && command -v fastfetch >/dev/null 2>&1 && fastfetch

# }}}

[ -n "${SHELLCUSTOMSTARTUPCOMMAND-}" ] && \
	eval "$SHELLCUSTOMSTARTUPCOMMAND"

# }}}
