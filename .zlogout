# Always exit cleanly in vscodium and Termux
if [ -n "${VSCODE_SHELL_INTEGRATION-}" ] \
|| [ -n "${TERMUX_PID-}" ]
then
	exit 0
fi
