#!/bin/sh

set -eu

cd "$HOME/.config/zsh/plugins/auto-ls"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi

cd "$HOME/.config/zsh/plugins/fast-syntax-highlighting"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi

cd "$HOME/.config/zsh/plugins/zsh-tab-title"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi

cd "$HOME/.config/zsh/plugins/zsh-autosuggestions"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi

cd "$HOME/.config/zsh/plugins/zsh-syntax-highlighting"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi

cd "$HOME/.config/zsh/plugins/ohmyzsh"
git fetch
if [ -n "$(git log HEAD..origin)" ]
then
	printf "\033[36m--- %s ---\033[0m\n" "$(pwd)"
	git -c color.ui=always pull --autostash
fi
