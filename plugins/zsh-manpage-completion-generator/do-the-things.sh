#!/bin/bash

# https://github.com/umlx5h/zsh-manpage-completion-generator

set -eu

WORKDIR="$(realpath "$(dirname "$0")")"

# generate fish manpage completions by default to
# $XDG_DATA_HOME/.local/share/fish/generated_man_completions
# or if $XDG_DATA_HOME isn't set then to
# $HOME/.local/share/fish/generated_man_completions
FISHCOMPLETIONDIR="${XDG_DATA_HOME-$HOME}/.local/share/fish/generated_completions"
mkdir -p "$FISHCOMPLETIONDIR"

cd "$WORKDIR"

export PATH="$WORKDIR:$PATH"

if command -pv fish
then
	# use fish to run its scripts if available
	fish -c 'fish_update_completions'
else
	# no fish? download its scripts
	URL="https://raw.githubusercontent.com/fish-shell/fish-shell/master/share/tools"
	wget --quiet -P "$WORKDIR" "$URL/create_manpage_completions.py" -O create_manpage_completions.py
	wget --quiet -P "$WORKDIR" "$URL/deroff.py"                     -O deroff.py
	chmod a+x "$WORKDIR"/{create_manpage_completions,deroff}.py

	# generate fish completions from manpages
	create_manpage_completions.py --manpath \
		--cleanup-in "$FISHCOMPLETIONDIR" \
		-d "$FISHCOMPLETIONDIR" --progress
fi

curl -sSL "https://github.com/umlx5h/zsh-manpage-completion-generator/releases/latest/download/zsh-manpage-completion-generator_$(uname -s)_$(uname -m).tar.gz" | tar xz zsh-manpage-completion-generator

chmod a+x "$WORKDIR/zsh-manpage-completion-generator"

# convert fish completions to zsh completions
# by default to $XDG_DATA_HOME/.local/share/zsh/generated_man_completions
# or if $XDG_DATA_HOME isn't set then to
# $HOME/.local/share/zsh/generated_man_completions
zsh-manpage-completion-generator
