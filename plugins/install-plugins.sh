#!/bin/bash -eu

#Clone zsh plugins, most of them are bundled in ohmyzsh

cd "$HOME"/.config/zsh/plugins/

git clone https://github.com/desyncr/auto-ls.git                        || true
git clone https://github.com/ohmyzsh/ohmyzsh.git                        || true
git clone https://github.com/trystan2k/zsh-tab-title.git                || true
git clone https://github.com/zdharma-continuum/fast-syntax-highlighting || true
git clone https://github.com/zsh-users/zsh-autosuggestions.git          || true
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git      || true

# Prompt/theme install
#cd "$HOME"/.config/zsh/plugins/
# try first with sudo, then do a local home dir install on error
#sudo starship/install.sh -y || {
#	mkdir -p "$HOME/.local/bin"
#	starship/install.sh -y -b "$HOME/.local/bin"
#}
"$HOME/.config/distro-setup/install/common/rust/starship.sh"
