# set up default settings
if (( ! ${+AUTO_LS_CHPWD} ))
then
	AUTO_LS_CHPWD=true
fi

if [[ $#AUTO_LS_COMMANDS -eq 0 ]]
then
	AUTO_LS_COMMANDS=(ls git-status)
fi

if (( ! ${+AUTO_LS_NEWLINE} ))
then
	AUTO_LS_NEWLINE=true
fi

if (( ! ${+AUTO_LS_PATH} ))
then
	AUTO_LS_PATH=true
fi


# set up default functions
auto-ls-ls () {
	ls --color=auto -a
	[[ $AUTO_LS_NEWLINE != false ]] && echo ""
}

# Yoink and slightly edit Oh-My-Zsh git-auto-fetch.plugin.zsh
# Default auto-fetch interval: 60 seconds
: ${GIT_AUTO_FETCH_INTERVAL:=60}
zmodload zsh/datetime
zmodload -F zsh/stat b:zstat  # only zstat command, not stat command
auto-ls-git-fetch-all () {
(
	# only fetch if prompt was empty
	if [[ ${WIDGET} != accept-line ]] || [[ $#BUFFER -ne 0 ]]
	then
		return 0
	fi

	# Get git root directory
	if ! gitdir="$(command git rev-parse --git-dir 2>/dev/null)"
	then
		return 0
	fi

	# Do nothing if auto-fetch is disabled or don't have permissions
	if [[ ! -w "$gitdir" || -f "$gitdir/NO_AUTO_FETCH" ]] \
	|| [[ -f "$gitdir/FETCH_LOG" && ! -w "$gitdir/FETCH_LOG" ]]
	then
		return 0
	fi

	# Get time (seconds) when auto-fetch was last run
	lastrun="$(zstat +mtime "$gitdir/FETCH_LOG" 2>/dev/null || echo 0)"
	# Do nothing if not enough time has passed since last auto-fetch
	if (( EPOCHSECONDS - lastrun < $GIT_AUTO_FETCH_INTERVAL ))
	then
		return 0
	fi

	# Fetch all remotes (avoid ssh passphrase prompt)
	date -R &>! "$gitdir/FETCH_LOG"
	GIT_SSH_COMMAND="command ssh -o BatchMode=yes" \
	GIT_TERMINAL_PROMPT=0 \
		command git fetch --all --recurse-submodules=yes \
			2>/dev/null &>> "$gitdir/FETCH_LOG"
) &|
}

auto-ls-git-status () {
	if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == true ]]
	then
		git status
	fi
}

# set up non-default functions
auto-ls-ls-hyperlink () {
	ls --color=auto -a --hyperlink=auto
	[[ $AUTO_LS_NEWLINE != false ]] && echo ""
}

auto-ls-git-diff-status () {
	if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == true ]]
	then
		# only show diff if prompt was empty
		if { [[ ${WIDGET} == accept-line ]] && [[ $#BUFFER -eq 0 ]] }
		then
			git diff
			echo ""
		fi
		git status
	fi
}


# main driver code
auto-ls () {
	# Possible invocation sources:
	#  1. Called from `chpwd_functions` – show file list
	#  2. Called by another ZLE plugin (like `dirhistory`) through `zle accept-line` – show file list
	#  3. Called by ZLE itself – only should file list if prompt was empty
	if ! zle \
	|| { [[ ${WIDGET} != accept-line ]] && [[ ${LASTWIDGET} != .accept-line ]] } \
	|| { [[ ${WIDGET} == accept-line ]] && [[ $#BUFFER -eq 0 ]] }
	then
		zle && echo
		for cmd in $AUTO_LS_COMMANDS
		do
			# If we detect a command with full path, ex: /bin/ls execute it
			if [[ $AUTO_LS_PATH != false && $cmd =~ '/' ]]
			then
				eval $cmd
			else
				# Otherwise run auto-ls function
				auto-ls-$cmd
			fi
		done
		zle && zle .accept-line
	fi

	# Forward this event down the ZLE stack
	if zle
	then
		if [[ ${WIDGET} == accept-line ]] && [[ $#BUFFER -eq 0 ]]
		then
			# Shortcut to reduce the number of empty lines appearing
			# when pressing Enter
			#echo && zle redisplay
			# this redisplay solution would duplicate the prompt
			# at least when using a starship prompt
			return
		elif [[ ${WIDGET} != accept-line ]] && [[ ${LASTWIDGET} == .accept-line ]]
		then
			# Hack to make only 2 lines appear after `dirlist` navigation
			# (Uses a VT100 escape sequence to move curser up one line…)
			tput cuu 1
		else
			zle .accept-line
		fi
	fi
}


# auto-ls when entering an empty line in prompt
zle -N auto-ls
zle -N accept-line auto-ls

# auto-ls when changing working directory
if [[ ${AUTO_LS_CHPWD} == true && ${chpwd_functions[(I)auto-ls]} -eq 0 ]]
then
	chpwd_functions+=(auto-ls)
fi
