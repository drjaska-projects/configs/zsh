# expand glob expressions, subcommands and aliases

#source ${ZSHPLUGINPATH}/ohmyzsh/plugins/globalias/globalias.plugin.zsh
globalias() {
	# Get last word to the left of the cursor:
	# (z) splits into words using shell parsing
	# (A) makes it an array even if there's only one element
	local word=${${(Az)LBUFFER}[-1]}
	if [[ $GLOBALIAS_FILTER_VALUES[(Ie)$word] -eq 0 ]]; then
		zle _expand_alias
		zle expand-word
	fi
	#zle self-insert # don't type ^@ if bound to ctr+space...
}
zle -N globalias
