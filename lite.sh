#!/bin/sh

set -eu

# This script copies most meaningful files from $ZDOTDIR to $HOME/.zsh
# $HOME/.zsh can then be used as a more mobile configuration set

mkdir -p "$HOME/.zsh"
BASEDIR="$HOME/.zsh"

#BASEDIR="$(mktemp -d)"
#trap 'rm -frv $BASEDIR' EXIT

for line in $(find "$(realpath $(dirname $0))/plugins" -not -iregex '.*\.\(gif\|md\|png\|jpg\|txt\)' -not -iregex '.*/\(AUTHORS\|NEWS\|LICENSE\)' -not -path "*.git*")
do
	[ -d "$line" ] && continue
	TARGETDIR="$BASEDIR/$(dirname $line | sed -e "s#$HOME/.config/zsh/##")"
	mkdir -p "$TARGETDIR"
	cp -f "$line" "$TARGETDIR"
done

cp -f "$(realpath $(dirname $0))/.zshrc" "$HOME/.zshrc"
