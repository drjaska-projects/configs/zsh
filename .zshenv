#!/bin/zsh

############################
#   XDG Base Directories   #
############################

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"

################
#   Env Vars   #
################

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

if [ -e /etc/os-release ]
then
	distro="$(grep '^ID=' /etc/os-release | cut -d '=' -f '2')"
	export distro

elif [ -n "$TERMUX_APP_PID" ]
then
	export distro="termux"
else
	export distro="unknown"
fi

export TIME_STYLE=long-iso
#export TIME_STYLE="%Y-%m-%d %H:%M:%S"

export GTK_THEME="Yaru:dark"
export GTK_THEME_VARIANT="dark"
export GTK2_RC_FILES="/usr/share/themes/Yaru-dark/gtk-2.0/gtkrc"

# Select EDITOR based on availability and preferences
if [ -x "$(which nvim)" ]
then
	export EDITOR="nvim"
else
	if [ -x "$(which vim)" ]
	then
		export EDITOR="vim"
	else
		if [ -x "$(which vi)" ]
		then
			export EDITOR="vi"
		else
			if [ -x "$(which micro)" ]
			then
				export EDITOR="micro"
			else
				if [ -x "$(realpath $(which editor))" ]
				then
					export EDITOR="editor"
				fi
			fi
		fi
	fi
fi

SYSTEMPATH+=("$PATH") # for diagnostics

prependtopath() {
	[ -d "$1" ] || return
	case ":$PATH:" in
		*:"$1":*)
			;;
		*)
			PATH="$1${PATH:+:$PATH}"
			;;
	esac
}

if [ "$distro" = "termux" ]
then
	prependtopath "$PREFIX/bin"
	prependtopath "$PREFIX/usr/bin"
	prependtopath "$PREFIX/sbin"
	prependtopath "$PREFIX/usr/local/games"
	prependtopath "$PREFIX/usr/games"
	prependtopath "$PREFIX/usr/sbin"
	prependtopath "$PREFIX/usr/local/bin"
	prependtopath "$PREFIX/usr/local/sbin"
else
	prependtopath "/bin"
	prependtopath "/usr/bin"
	prependtopath "/sbin"
	prependtopath "/usr/local/games"
	prependtopath "/usr/games"
	prependtopath "/usr/sbin"
	prependtopath "/usr/local/bin"
	prependtopath "/usr/local/sbin"
fi

prependtopath "$XDG_DATA_HOME"/cargo/bin
prependtopath "$HOME/.local/bin"
prependtopath "$HOME/bin"
prependtopath "$HOME/bbin"

# In the case of SSH use and prefer ssh kitten's kitty/kitten bins
[ -n "${SSH_CONNECTION-}" ] && \
	prependtopath "$HOME/.local/share/kitty-ssh-kitten/kitty/bin"

export PATH
unset -f prependtopath

# ghcup-env
#[ -e "$HOME/.ghcup/env" ] && \
#	source "$HOME/.ghcup/env"

[ -e "$XDG_CONFIG_HOME/homefolder/lesskey" ] && \
	export LESSKEYIN="$XDG_CONFIG_HOME/homefolder/lesskey"

###################################################
#   Env Vars For Following XDG Base Directories   #
###################################################

# [cargo]: ~/.cargo
[ -e "$HOME/.cargo" ] && \
	mv "$HOME/.cargo" "$XDG_DATA_HOME/cargo"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

# [rustup]: ~/.rustup
[ -e "$HOME/.rustup" ] && \
	mv "$HOME/.rustup" "$XDG_DATA_HOME/rustup"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# [cuda]: ~/.nv
[ -e "$HOME/.nv" ] && \
	mv "$HOME/.nv" "$XDG_CACHE_HOME/nv"
export CUDA_CACHE_PATH="$XDG_CACHE_HOME/nv"

# [gtk-2]: ~/.gtkrc-2.0
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"

# [ncurses]: ~/.terminfo
# Doesn't seem to be movable
#[ -e "$HOME/.terminfo" ] && \
#	mv "$HOME/.terminfo" "$XDG_DATA_HOME"/terminfo
#export TERMINFO="$XDG_DATA_HOME"/terminfo
#export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo

# [nss]: ~/.pki
# Doesn't seem to be movable
#[ -e "$HOME/.pki" ] && \
#	mv "$HOME/.pki" "$XDG_DATA_HOME/pki"
